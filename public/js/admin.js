"use strict";

const deleteProduct = ( el ) => {
    const productId = el.parentNode.querySelector( "[name='productId']" ).value;
    const csrf      = el.parentNode.querySelector( "[name='_csrf']" ).value;

    fetch( "/admin/product/" + productId, {
        method: "DELETE",
        headers: {
            "csrf-token": csrf
        }
    }).then( res => {
        return res.json();
    }).then( data => {
        console.log( data );
        el.closest( "article" ).remove();
    }).catch( err => {
        console.log( err );
    })
}
