const path              = require( "path" );

const express           = require( "express" );
const { body }          = require( "express-validator/check" );

const adminController   = require( path.join( __rootPath, "controllers", "admin" ));
const isAuth            = require( path.join( __rootPath, "middleware", "is-auth" ));

const router = express.Router();

//Requests go through handlers from left to right.

// /admin/products => GET
router.get( "/products", isAuth,                adminController.getProducts );

// /admin/add-product => GET
router.get( "/add-product", isAuth,             adminController.getAddProduct );

// /admin/add-product => POST
router.post( "/add-product", isAuth, [
    body( "title" ).isString().isLength({ min: 3 }).trim(),
    body( "price" ).isFloat(),
    body( "description" ).isLength({ min: 10, max: 400 }).trim()
],
                                                adminController.postAddProduct );

// /admin/edit-product/:productId => GET
router.get( "/edit-product/:productId", isAuth, adminController.getEditProduct );

// /admin/edit-product => POST
router.post( "/edit-product", isAuth, [
    body( "title" ).isString().isLength({ min: 3, max:50 }).trim().withMessage( "Title has to be between 3 and 50 characters" ),
    body( "price" ).isFloat().withMessage( "Price has to contain decimal XX.YY" ),
    body( "description" ).isLength({ min: 10, max: 400 }).trim()
],
                                                adminController.postEditProduct );

// /admin/product/:productId => DELETE
router.delete( "/product/:productId", isAuth,   adminController.deleteProduct );


module.exports = router;
