const path              = require( "path" );

const express           = require( "express" );

const shopController    = require( path.join( __rootPath, "controllers", "shop" ));
const isAuth            = require( path.join( __rootPath, "middleware", "is-auth" ));

const router            = express.Router();

// / => GET
router.get( "/",                                    shopController.getIndex );

// /products => GET
router.get( "/products",                            shopController.getProducts );

// /products/:id => GET
router.get( "/products/:productId",                 shopController.getProduct );

// /cart => GET
router.get( "/cart", isAuth,                        shopController.getCart );

// /cart => POST
router.post( "/cart", isAuth,                       shopController.postCart );

// /cart-delete-item => POST
router.post( "/cart-delete-item", isAuth,           shopController.postCartDeleteProduct );

// /submit-order => POST
router.post( "/submit-order", isAuth,               shopController.postOrder );

// /orders => GET
router.get( "/orders", isAuth,                      shopController.getOrders );

// /orders/:orderId => GET
router.get( "/orders/:orderId", isAuth,             shopController.getInvoice );

// /checkout => GET
router.get( "/checkout",                            shopController.getCheckout );

module.exports = router;
