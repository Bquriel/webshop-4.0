const path      = require( "path" );

const express           = require( "express" );
const { check, body }   = require( "express-validator/check" );

const authController    = require( path.join( __rootPath, "controllers", "auth" ));
const User              = require( path.join( __rootPath, "models", "user" ));

const router            = express.Router();

// /signup => GET
router.get( "/signup",                              authController.getSignup );

// /signup => POST
router.post( "/signup",
    check( "email" ).isEmail().withMessage( "Please enter a valid email")      //Can be chained. withMessage will alway be for previous validation. Looks for "email" in forms, body, params, cookies etc.. Only specific part like body can be imported to look from one source.
        .custom(( value, { req }) => {                                          //Own validator can be set up this way. True should be returned if test passes and throw new Error(msg) if fails.
            return User.findOne({ email: value })
                .then( user => {
                    if( user ){
                        return Promise.reject( "User with this email already exists" ); //Throws error inside Promise.
                    }
                })
        }),
        body( "password", `Please enter a password of over ${ __config.password.minLength } characters` )   //Second argument serves as message for all cases. If promise fulfills, validator will treat as succesfull. If reject, then fail.
        .isLength({ min: __config.password.minLength }),
        body( "confirmPassword" ).custom(( value, { req }) => {
            if( value !== req.body.password )
                throw new Error( "Passwords have to match" );                   //Validator catches error and adds it to error array.
            return true;
        }),
                                                    authController.postSignup );

// /login => GET
router.get( "/login",                               authController.getLogin );

// /login => POST
router.post( "/login",
    body( "email" ).isEmail().withMessage( "Please enter a valid email" ),
                                                    authController.postLogin );

// /logout => POST
router.post( "/logout",                             authController.postLogout );

// /reset => GET
router.get( "/reset",                               authController.getReset );

// /reset => POST
router.post( "/reset",                              authController.postReset );

// /reset/:token => GET
router.get( "/reset/:token",                        authController.getNewPassword );

// /new-password => POST
router.post( "/new-password",                       authController.postNewPassword );

module.exports = router;
