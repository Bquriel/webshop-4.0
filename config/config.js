const _                 = require( "lodash" );

const config            = require( "./config.json" );
const defaultConfig     = config.development;
const environment       = process.env.NODE_ENV || 'development';
const environmentConfig = config[ environment ];
const finalConfig       = _.merge( defaultConfig, environmentConfig );

envConfig = {
    port: process.env.PORT || finalConfig.port,
    MongoDB: {
        uri: process.env.MONGO_URI,
        user: process.env.MONGO_USER,
        password: process.env.MONGO_PASSWORD,
        database: process.env.MONGO_DATABASE
    },
    stripe: {
        key: process.env.STRIPE_KEY
    }
}

global.__config         = finalConfig;

module.exports          = finalConfig;
