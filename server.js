//Encrypt config at push and decrypt during CI/CD deployment.
//Globals
global.__rootPath       = __dirname;
global.__renderEngine   = "pug";

console.log( "Loading packages.." );

const path              = require( "path" );
const fs                = require( "fs" );
//const https             = require( "https" );

const express           = require( "express" );
const bodyParser        = require( "body-parser" ).urlencoded({ extended: false });
const session           = require( "express-session" );
const MongoDBStore      = require( "connect-mongodb-session" )( session );
const expressHbs        = require( "express-handlebars" );                      //Only if hbs is used.
const mongoose          = require( "mongoose" );
const csrf              = require( "csurf" );
const flash             = require( "connect-flash" );
const helmet            = require( "helmet" );
const compression       = require( "compression" );
//const morgan            = require( "morgan" );

const User              = require( path.join( __dirname, "models", "user" ));
const config            = require( path.join( __dirname, "config", "config" ));
const multer            = require( path.join( __dirname, "middleware", "multer" ));

const errorController   = require( path.join( __dirname, "controllers", "error.js" ));
const adminRoutes       = require( path.join( __dirname, "routes", "admin.js" ));
const shopRoutes        = require( path.join( __dirname, "routes", "shop.js" ));
const authRoutes        = require( path.join( __dirname, "routes", "auth.js" ));

console.log( "Initializing server.." );

//Creates folders for dynamic data. With recursive doesn't make dir if it exists and makes subdirs also.
fs.mkdirSync( path.join( __dirname, "img" ), { recursive: true });
fs.mkdirSync( path.join( __dirname, "data", "invoices" ), { recursive: true });

const app               = express();

switch( __renderEngine ){
    case "pug":                                                                 //Pug is far away from original html and supports template side logic. Supports many layouts.
        app.set( "view engine", "pug" );                                        //Sets global configuration value. Express has reserved keys that change the way app behaves.
        app.set( "views", "views/pug" );                                        //Reserved keyword for templates that defaults to views folder, so this line is redundant.
        break;

    case "hbs":                                                                 //Hbs is close to html and doesn't support template side logic. Supports single layout.
        /**
         * Registers new engine. File endings (.file) are same as registered name. If name was handlebars, the ending would be .handlebars.
         * @String custom name that should not clash with pre-registered engines like pug.
         * @Function tool that express will use as engine. Usually import. Should return initialized engine.
         */
        app.engine( "hbs", expressHbs({
            layoutsDir: "views/hbs/layouts/",                                   // /views/layouts is the default.
            defaultLayout: "main-layout",                                       //DefaultLayout is optional.
            extname: "hbs",                                                     //Extension name for layouts that defaults to .handlebars.
            helpers: {                                                          //Adds operators to hbs views.
                    "ifCond": function ( v1, operator, v2, options ) {          //{{#ifCond cond1 "operator" cond2}}
                    switch ( operator ) {
                        case '==':
                            return (v1 == v2)   ? options.fn(this) : options.inverse(this);
                        case '===':
                            return (v1 === v2)  ? options.fn(this) : options.inverse(this);
                        case '!=':
                            return (v1 != v2)   ? options.fn(this) : options.inverse(this);
                        case '!==':
                            return (v1 !== v2)  ? options.fn(this) : options.inverse(this);
                        case '<':
                            return (v1 < v2)    ? options.fn(this) : options.inverse(this);
                        case '<=':
                            return (v1 <= v2)   ? options.fn(this) : options.inverse(this);
                        case '>':
                            return (v1 > v2)    ? options.fn(this) : options.inverse(this);
                        case '>=':
                            return (v1 >= v2)   ? options.fn(this) : options.inverse(this);
                        case '&&':
                            return (v1 && v2)   ? options.fn(this) : options.inverse(this);
                        case '||':
                            return (v1 || v2)   ? options.fn(this) : options.inverse(this);
                        default:
                            return options.inverse(this);
                    }
                }
            }
        }));
        app.set( "view engine", "hbs" );
        app.set( "views", "views/hbs" );
        break;

    case "ejs":                                                                 //Ejs is close to html and supports template side logic. Doesn't support layouts.
        app.set( "view engine", "ejs" );
        app.set( "views", "views/ejs" );
        break;

    default:
        console.error( "Rendering engine needs to be registered!" );
        process.exit( -1 );
}

const csrfProtection    = csrf();
/* For own HTTPS. NGINX reverse-proxy used instead.
const privateKey    = fs.readFileSync( __rootPath, "server.key" );
const certificate   = fs.readFileSync( __rootPath, "sever.cert" );
*/

const store = new MongoDBStore({
    uri: config.mongoDB.uri,
    collection: "sessions"
});


app.use( express.static( path.join( __rootPath, "public" )));
app.use( "/img", express.static( path.join( __rootPath, "img" )));

app.use( bodyParser );

//Don't use as a global middleware like this to avoid malicious uploads. Use only on routes that handle files.
//TODO move to each route separately with csrf following afterwards.
app.use( multer );


app.use( session({                                                              //Sessions should be stored in database.
    secret: "AIOj389ruj3298rjsdkfa!¤<z>tgiorp3et39543",                         //Secret will sign hash that stores id in cookie.
    resave: false,                                                              //Session will be resaved only if something changes.
    saveUninitialized: false,                                                   //Session will be saved only when needed.
    store: store                                                                //Where cookies are stored. Defaults to in momory which is not for production. Local store is always faster than remote and redis is faster than mongodb.
    //cookie: {}                                                                //Regular cookie variables {maxAge, expires, ...}.
}));
app.use(( req, res, next ) => {
    if( req.url !== "/submit-order" ){
        app.use( csrfProtection );                                              //Relies on session. Looks for tokens in view for any POST-request. All form parsers have to be used before this.
    }
    next();
})
app.use( flash());                                                              //Relies on session. Used to flash data to session for one-time use. Good for error messages. Data is deleted once pulled.

//Adds many secure response headers.
app.use( helmet());
//Compresses response. Don't use in case hosting provider does it. Heroku doesn't do it.
app.use( compression());
/*
const accessLogStream = fs.createWriteStream( path.join( __rootPath, "access.log" ),
    {
        flags: "a"                                                              //Appends logs.
    }
);
//Used for logging request data (NGINX etc do this automatically)
app.use( morgan( "combined", { stream: accessLogStream }));
*/

//Csrf token should be set before any possible error contexts like getting users. Otherwise pages will fail.
app.use(( req, res, next ) => {
    res.locals.isAuthenticated  = req.session.isLoggedIn;                       //res.locals values go to each view that is rendered.
    res.locals.csrfToken = req.csrfToken ? req.csrfToken() : "";
    next();
});

app.use(( req, res, next ) => {
    if( !req.session.user )
        return next();
    User.findById( req.session.user._id )
        .then( user => {
            if( !user ){
                return next();
            }
            req.user = user;
            next();
        })
        .catch( err => {
            next( new Error( err ));                                            //Throwing error in async context won't reach error-middleware (with 4 args). Outside of asynt will reach.
        });
});

//Routes
app.use( "/admin", adminRoutes );
app.use( shopRoutes );
app.use( authRoutes );

app.get( "/500", errorController.get500 );
app.use( errorController.get404 );

//Errors given to next( error ) will go here.
app.use(( error, req, res, next ) => {
    // res.status( error.httpStatusCode ).render(...);
    // res.redirect( "/500" );
    console.error( error );
    errorController.get500( req, res, next );
});

console.log( "Connecting to database.." );

mongoose.connect( config.mongoDB.uri, { useNewUrlParser: true })
    .then( result => {
        //process.env.port
        app.listen( __config.port );
        /* HTTPS server
        https.createServer({ key: privateKey, cert: certificate }, app ).listen( __config.port );
        */
        console.log( "App started on port:", __config.port, "in", process.env.NODE_ENV, "mode" );
    })
    .catch( err => console.log( err ));
