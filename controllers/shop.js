const path          = require( "path" );
const fs            = require( "fs" );

const PDFDocument   = require( "pdfkit" );
const stripe        = require( "stripe" )( __config.stripe.key );

const Product       = require( path.join( __rootPath, "models", "product" ));
const Order         = require( path.join( __rootPath, "models", "order" ));

const ITEMS_PER_PAGE = 1;

// / => GET
exports.getIndex = ( req, res, next ) => {
    const page = +req.query.page || 1;
    let totalItems;

    Product.find().countDocuments()
        .then( productsNumber => {
            totalItems = productsNumber;
            return Product.find()
                    .skip(( page - 1 ) * ITEMS_PER_PAGE )                       //Skips amount of items
                    .limit( ITEMS_PER_PAGE )
        })
        .then( products => {
            switch( __renderEngine ){
                case "pug":
                case "ejs":
                    res.render( "shop/index", {
                        pageTitle: "Shop",
                        path: "/",
                        products: products,
                        currentPage: page,
                        hasNextPage: page * ITEMS_PER_PAGE < totalItems,
                        hasPreviousPage: page > 1,
                        nextPage: page + 1,
                        previousPage: page - 1,
                        lastPage: Math.ceil( totalItems/ITEMS_PER_PAGE )
                    });
                    break;

                case "hbs":
                    res.render( "shop/index", {
                        pageTitle: "Shop",
                        products: products,
                        hasProducts: products.length > 0,
                        activeShop: true,
                        productCSS: true,
                        currentPage: page,
                        hasNextPage: page * ITEMS_PER_PAGE < totalItems,
                        hasPreviousPage: page > 1,
                        nextPage: page + 1,
                        previousPage: page - 1,
                        lastPage: Math.ceil( totalItems/ITEMS_PER_PAGE ),
                        displayFirstPage: page !== 1 && ( page - 1 ) !== 1,
                        displayLastPage: Math.ceil( totalItems/ITEMS_PER_PAGE ) !== page && Math.ceil( totalItems/ITEMS_PER_PAGE ) !== ( page + 1 )
                    });
                    break;

                default:
                    console.error( "Render engine must be specified!" );
            }
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};


// /products => GET
exports.getProducts = ( req, res, next ) => {
    const page = +req.query.page || 1;
    let totalItems;

    Product.find().countDocuments()
        .then( productsNumber => {
            totalItems = productsNumber;
            return Product.find()
                    .skip(( page - 1 ) * ITEMS_PER_PAGE )
                    .limit( ITEMS_PER_PAGE )
        })
        .then( products => {
            //Renders file using default templating engine. Can be defined with app.set( "view engine", name ). Second argument is key-value pairs for dynamic rendering.
            //Pug supports conditionals in page randering. handlebars doesn't.
            switch( __renderEngine ){
                case "pug":
                case "ejs":
                    res.render( "shop/product-list", {
                        pageTitle: "All Products",
                        path: "/products",
                        products: products,
                        currentPage: page,
                        hasNextPage: page * ITEMS_PER_PAGE < totalItems,
                        hasPreviousPage: page > 1,
                        nextPage: page + 1,
                        previousPage: page - 1,
                        lastPage: Math.ceil( totalItems/ITEMS_PER_PAGE )
                    });
                    break;

                case "hbs":
                    res.render( "shop/product-list", {
                        pageTitle: "All Products",
                        products: products,
                        hasProducts: products.length > 0,
                        activeProducts: true,
                        productCSS: true,
                        currentPage: page,
                        hasNextPage: page * ITEMS_PER_PAGE < totalItems,
                        hasPreviousPage: page > 1,
                        nextPage: page + 1,
                        previousPage: page - 1,
                        lastPage: Math.ceil( totalItems/ITEMS_PER_PAGE ),
                        displayFirstPage: page !== 1 && ( page - 1 ) !== 1,
                        displayLastPage: Math.ceil( totalItems/ITEMS_PER_PAGE ) !== page && Math.ceil( totalItems/ITEMS_PER_PAGE ) !== ( page + 1 )
                        //layout: false                                                 //Defines that default layout should not be used.
                    });
                    break;

                default:
                    console.error( "Render engine must be specified!" );
            }
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};



// /products/:productId => GET
exports.getProduct = ( req, res, next ) => {
    const id = req.params.productId;
    //If img url is local, then this request will fire also for the image causing the product to be undefined. Absolute url's should be enforced!
    Product.findById( id )
        .then( product => {
            //Temporary solution to comment above.
            if( typeof product === "undefined" ){
                return next();
            }
            switch( __renderEngine ){
                case "pug":
                case "ejs":
                    res.render( "shop/product-detail", {
                        pageTitle: product.title,
                        path: "/products",
                        product: product
                    });
                    break;

                case "hbs":
                    res.render( "shop/product-detail", {
                        pageTitle: product.title,
                        product: product,
                        productCSS: true,
                        activeProducts: true
                    });
                    break;

                default:
                    console.error( "Render engine must be specified!" );
            }
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};


// /cart => GET
exports.getCart = ( req, res, next ) => {
    req.user.populate( "cart.items.productId" ).execPopulate()                  //execPopulate() will return a promise.
        .then( user => {
            const products = user.cart.items;
            switch( __renderEngine ){
                case "pug":
                case "ejs":
                    res.render( "shop/cart", {
                        pageTitle: "Your Cart",
                        path: "/cart",
                        products: products
                    });
                    break;

                case "hbs":
                    res.render( "shop/cart", {
                        pageTitle: "Your Cart",
                        activeCart: true,
                        cartCSS: true,
                        products: products,
                        hasProducts: products.length > 0
                    });
                    break;

                default:
                    console.error( "Render engine must be specified!" );
            }
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};



// /cart => POST
exports.postCart = ( req, res, next ) => {
    const id = req.body.productId;
    Product.findById( id )
        .then( product => {
            return req.user.addToCart( product )
        })
        .then( result => {
            res.redirect( "/cart" );
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};


// /cart-delete-item => POST
exports.postCartDeleteProduct = ( req, res, next ) => {
    const id = req.body.productId;
    req.user.deleteItemFromCart( id )
    .then( result => {
        res.redirect( "/cart" );
    })
    .catch( err => {
        const error = new Error( err );
        error.httpStatusCode = 500;
        return next( error );
    });
};


// /checkout => GET
exports.getCheckout = ( req, res, next ) => {
    req.user.populate( "cart.items.productId" ).execPopulate()                  //execPopulate() will return a promise.
        .then( user => {
            const products  = user.cart.items;
            let total       = 0;
            for( let product of products ){
                total += product.quantity * product.productId.price;
            }
            switch( __renderEngine ){
                case "pug":
                case "ejs":
                    res.render( "shop/checkout", {
                        pageTitle: "Checkout",
                        path: "",
                        products: products,
                        totalSum: total
                    });
                    break;

                case "hbs":
                    res.render( "shop/checkout", {
                        pageTitle: "Checkout",
                        cartCSS: true,
                        products: products,
                        hasProducts: products.length > 0,
                        totalSum: total,
                        totalSumStripe: total * 100
                    });
                    break;

                default:
                    console.error( "Render engine must be specified!" );
            }
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};


// /submit-order => POST
exports.postOrder = ( req, res, next ) => {
    const stripeToken = req.body.stripeToken;
    let totalPrice = 0;

    req.user.populate( "cart.items.productId" ).execPopulate()                  //execPopulate() will return a promise.
        .then( user => {
            for( let product of user.cart.items ){
                totalPrice += product.quantity * product.productId.price;
            }
            const products = user.cart.items.map( i => { return { quantity: i.quantity, product: { ...i.productId._doc }}});    //._doc doesn't give metadata.
            const order = new Order({
                user: {
                    email: req.user.email,
                    userId: req.user._id
                },
                products: products
            });
            return order.save();
        })
        .then( result => {
            const charge = stripe.charges.create({
                amount: totalPrice * 100,
                currency: "eur",
                description: "Test charge",
                source: stripeToken,
                metadata: {
                    orderId: result._id.toString()
                }
            });
            return req.user.clearCart();
        })
        .then( result => {
            res.redirect( "/orders" );
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};



// /orders => GET
exports.getOrders = ( req, res, next ) => {
    Order.find({ "user.userId": req.user._id })
    .then( orders => {
        switch( __renderEngine ){
            case "pug":
            case "ejs":
                res.render( "shop/orders", {
                    pageTitle: "Your Orders",
                    path: "/orders",
                    orders: orders
                });
                break;

            case "hbs":
                res.render( "shop/orders", {
                    pageTitle: "Your Orders",
                    activeOrders: true,
                    ordersCSS: true,
                    orders: orders,
                    hasOrders: orders.length > 0
                });
                break;

            default:
                console.error( "Render engine must be specified!" );
        }
    })
    .catch( err => {
        const error = new Error( err );
        error.httpStatusCode = 500;
        return next( error );
    });
};


// /invoice/:orderId => GET
exports.getInvoice = ( req, res, next ) => {
    const orderId       = req.params.orderId;
    Order.findById( orderId )
        .then( order => {
            if( !order ){
                return next( new Error( "No order found!" ));
            }
            if( order.user.userId.toString() !== req.user._id.toString()) {
                return next( new Error( "Unauthorized" ));                      //TODO: Show custom error message.
            }
            const invoiceName   = "invoice-" + orderId + ".pdf";
            const invoicePath   = path.join( "data", "invoices", invoiceName );

            res.setHeader( "Content-Type", "application/pdf" );
            res.setHeader( "Content-Disposition", "attachment; filename=" + invoiceName );

            const pdfDoc        = new PDFDocument();                            //Is readable stream.
            pdfDoc.pipe( fs.createWriteStream( invoicePath ));
            pdfDoc.pipe( res );

            pdfDoc.fontSize( 26 ).text( "Invoice", {
                underline: true
            });
            pdfDoc.text( "-----------------------" );

            let totalPrice = 0;
            order.products.forEach( productOrder => {
                pdfDoc.fontSize( 14 ).text( `${ productOrder.product.title } - ${ productOrder.quantity } x ${ productOrder.product.price }€` );
                totalPrice += productOrder.quantity * productOrder.product.price;
            });
            pdfDoc.text( "------" );
            pdfDoc.fontSize( 18 ).text( `Total Price: ${ totalPrice }€` );
            pdfDoc.end();                                                       //Ends writing to stream. File will be saved and response sent.

            // readFile reads file completely to memory before sending it, which can overflow memory with many requests. STREAM INSTEAD.
            // fs.readFile( invoicePath, ( err, file ) => {
            //     if( err ){
            //         return next( err );
            //     }
            //     res.setHeader( "Content-Type", "application/pdf" );                     //With this opens the file in browser
            //
            //     //How the content should be served. Inline is default a.k.a opening in browser. Attachment downloads by default.
            //     res.setHeader( "Content-Disposition", "attachment; filename='" + invoiceName + "'" );
            //     res.send( file );
            // })

            // Better way of sending file.
            // const file = fs.createReadStream( invoicePath );
            // res.setHeader( "Content-Type", "application/pdf" );
            // res.setHeader( "Content-Disposition", "attachment; filename='" + invoiceName + "'" );
            // file.pipe( res );                                                   //Res is a writeable stream, so readable streams can forward data to writeable streams.
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};
