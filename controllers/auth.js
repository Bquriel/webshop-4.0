const path                  = require( "path" );
const crypto                = require( "crypto" );

const bcrypt                = require( "bcryptjs" );
const nodemailer            = require( "nodemailer" );
const { validationResult}   = require( "express-validator/check" );

const User                  = require( path.join( __rootPath, "models", "user" ));

const transporter = nodemailer.createTransport({
    host: "smtp.sendgrid.net",
    port: 465,
    auth: {
        user: "apikey",
        pass: "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"
    }
});

// /signup => GET
exports.getSignup = ( req, res, next ) => {
    let message = req.flash( "error" );
    message = message.length ? message[ 0 ] : null;
    switch( __renderEngine ){
        case "pug":
        case "ejs":
            res.render( "auth/signup", {
                pageTitle: "Register",
                path: "/signup",
                errorMessage: message,
                oldInput: {
                    firstName: "",
                    lastName: "",
                    email: ""
                },
                validationErrors: []
            });
            break;

        case "hbs":
            res.render( "auth/signup", {
                pageTitle: "Register",
                authCSS: true,
                formsCSS: true,
                activeSignup: true,
                hasError: message != null,
                errorMessage: message,
                oldInput: {
                    firstName: "",
                    lastName: "",
                    email: ""
                },
                validationErrors: []
            });
            break;

        default:
            console.error( "Render engine must be specified!" );
    }
}



// /signup => POST
exports.postSignup = ( req, res, next ) => {
    const firstName         = req.body.firstName;
    const lastName          = req.body.lastName;
    const email             = req.body.email;
    const password          = req.body.password;
    const errors            = validationResult( req );
    if( !errors.isEmpty()){
        const errorsArray = errors.array();
        switch( __renderEngine ){
            case "pug":
            case "ejs":
                return res.status( 422 ).render( "auth/signup", {               //Response code for failed validation.
                    pageTitle: "Register",
                    path: "/signup",
                    errorMessage: errorsArray[ 0 ].msg,
                    oldInput: {
                        firstName: firstName,
                        lastName: lastName,
                        email: email
                    },
                    validationErrors: {
                        email:              errorsArray.find( e => e.param === "email" )              ? "invalid" : "",
                        password:           errorsArray.find( e => e.param === "password" )           ? "invalid" : "",
                        confirmPassword:    errorsArray.find( e => e.param === "confirmPassword" )    ? "invalid" : ""
                    }
                });
                break;

            case "hbs":
                return res.status( 422 ).render( "auth/signup", {
                    pageTitle: "Register",
                    authCSS: true,
                    formsCSS: true,
                    activeSignup: true,
                    hasError: !errors.isEmpty(),
                    errorMessage: errorsArray[ 0 ].msg,
                    oldInput: {
                        firstName: firstName,
                        lastName: lastName,
                        email: email
                    },
                    validationErrors: {
                        email:              errorsArray.find( e => e.param === "email" )              ? "invalid" : "",
                        password:           errorsArray.find( e => e.param === "password" )           ? "invalid" : "",
                        confirmPassword:    errorsArray.find( e => e.param === "confirmPassword" )    ? "invalid" : ""
                    }
                });
                break;

            default:
                console.error( "Render engine must be specified!" );
        }
    }

    bcrypt.hash( password, __config.password.saltStrength )                              //String to hash and salt value - how many rounds of hashing. 12 is concidered secure as of 2019
        .then( hashedPassword => {
            return User({ firstName: firstName, lastName: lastName, email: email, password: hashedPassword, cart: { items: []}}).save();
        })
        .then( result => {
            res.redirect( "/login" );/*
            return transporter.sendMail({
                to: email,
                from: "no-reply@bestshop.com",
                subject: "Welcome to best shop",
                html: "<h1>You have succesfully signed up to the webshop</h1><p>Welcome to use our services</p><br><p>With regards,<br><i>-The shop team</i></p>"
            });*/
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
}


//WITH CHANGES DOES THE ERROR MESSAGE APPEA ANYMORE IN FLASH?
// /login => GET
exports.getLogin = ( req, res, next ) => {
    let message = req.flash( "error" );
    message = message.length ? message[ 0 ] : null;
    switch( __renderEngine ){
        case "pug":
        case "ejs":
            res.render( "auth/login", {
                pageTitle: "Login",
                path: "/login",
                errorMessage: message,
                oldInput: {
                    email: ""
                }
            });
            break;

        case "hbs":
            res.render( "auth/login", {
                pageTitle: "Login",
                authCSS: true,
                formsCSS: true,
                activeLogin: true,
                hasError: message != null,
                errorMessage: message,
                oldInput: {
                    email: ""
                }
            });
            break;

        default:
            console.error( "Render engine must be specified!" );
    }
}



// /login => POST
// @param email
// @param password
exports.postLogin = ( req, res, next ) => {
    //Expires: will expire when browser is closed if not set.
    //Max-Age: how many seconds cookie should exist
    //Domain: to which domain cookie should be sent
    //Secure: cookie will be set only if page is served via https
    //HttpOnly: cannot be accessed by js - GOOD FOR SECURITY
    //res.setHeader( "Set-Cookie", "authenticated=true; Expires=asd; Max-Age=asd; Domain=asd; Secure; HttpOnly );
    const email     = req.body.email;
    const password  = req.body.password;
    //MongDB store has sessions and fetches user from there. It doesn't know about mongoose so model methods are not valid. User has to be retrieved by middleware etc.
    const errors    = validationResult( req );
    if( !errors.isEmpty()){
        return renderPostLogin( res, email, errors.array()[ 0 ].msg);
    }
    User.findOne({ email: email })
        .then( user => {
            if( !user ){
                return renderPostLogin( res, email, "Invalid email or password");
            }
            bcrypt.compare( password, user.password )                           //Then block is reached in mathing and non-matching case.
                .then( doMatch => {
                    if( doMatch ){
                        req.session.isLoggedIn  = true;
                        req.session.user        = user;
                        return req.session.save( err => {                       //Usually not needed, but saving to database takes time and too early redirect may cause problems, so it's good to wait.
                            if( err ){
                                console.log( err );
                            }
                            res.redirect( "/" );
                        });
                    }
                    return renderPostLogin( res, email, "Invalid email or password" );
                })
                .catch( err => {
                console.log( err );
                res.redirect( "/login" );
            })
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
}

const renderPostLogin = ( res, email, errorMessage ) => {
    switch( __renderEngine ){
        case "pug":
        case "ejs":
            return res.status( 422 ).render( "auth/login", {
                pageTitle: "Register",
                path: "/login",
                errorMessage: errorMessage,
                oldInput: {
                    email: email
                }
            });
            break;

        case "hbs":
            return res.status( 422 ).render( "auth/login", {
                pageTitle: "Register",
                authCSS: true,
                formsCSS: true,
                activeSignup: true,
                hasError: true,
                errorMessage: errorMessage,
                oldInput: {
                    email: email
                }
            });
            break;

        default:
            console.error( "Render engine must be specified!" );
    }
}



// /logout => POST
exports.postLogout = ( req, res, next ) => {
    req.session.destroy( err => {
        if( err )
            console.log( err );
        res.redirect( "/" );
    });
}


// /reset => GET
exports.getReset = ( req, res, next ) => {
    let message = req.flash( "error" );
    message = message.length ? message[ 0 ] : null;
    switch( __renderEngine ){
        case "pug":
        case "ejs":
            res.render( "auth/reset", {
                pageTitle: "Reset Password",
                path: "/reset",
                errorMessage: message
            });
            break;

        case "hbs":
            res.render( "auth/reset", {
                pageTitle: "Reset Password",
                authCSS: true,
                formsCSS: true,
                hasError: message != null,
                errorMessage: message
            });
            break;

        default:
            console.error( "Render engine must be specified!" );
    }
}


// /reset => POST
exports.postReset = ( req, res, next ) => {
    crypto.randomBytes( 32, ( err, buffer ) => {
        if( err ){
            console.log( err );
            return res.redirect( "/reset" );
        }
        const token = buffer.toString( "hex" );
        User.findOne({ email: req.body.email })
            .then( user => {
                if( !user ){
                    req.flash( "error", "No account with specified email found" );
                    return res.redirect( "/reset" );
                }
                user.resetToken = token;
                user.resetTokenExpiration = Date.now() + 3600000;               //1 hour
                return user.save();
            })
            .then( result => {
                res.redirect( "/" );
                transporter.sendMail({
                    to: req.body.email,
                    from: "no-reply@bestshop.com",
                    subject: "Password reset",
                    html: `
                        <h1>Reset password</h1>
                        <p>You have requested a password reset.</p>
                        <p>Click <a href="http://localhost:${ __config.port }/reset/${ token }">here</a> to set a new password.</p>
                    `
                });
            })
            .catch( err => {
                const error = new Error( err );
                error.httpStatusCode = 500;
                return next( error );
            });
    });
}



// /reset/:token => GET
exports.getNewPassword = ( req, res, next ) => {
    const token = req.params.token;
    User.findOne({ resetToken: token, resetTokenExpiration: { $gt: Date.now() }})
    .then( user => {
        if( !user ){
            res.redirect( "/" );
        }
        let message = req.flash( "error" );
        message = message.length ? message[ 0 ] : null;
        switch( __renderEngine ){
            case "pug":
            case "ejs":
            res.render( "auth/new-password", {
                pageTitle: "New Password",
                path: "/new-password",
                userId: user._id.toString(),
                passwordToken: token,
                errorMessage: message
            });
            break;

            case "hbs":
            res.render( "auth/new-password", {
                pageTitle: "New Password",
                authCSS: true,
                formsCSS: true,
                userId: user._id.toString(),
                passwordToken: token,
                hasError: message != null,
                errorMessage: message
            });
            break;

            default:
            console.error( "Render engine must be specified!" );
        }
    })
    .catch( err => {
        const error = new Error( err );
        error.httpStatusCode = 500;
        return next( error );
    });
}



// /new-password => POST
exports.postNewPassword = ( req, res, next ) => {
    const newPassword   = req.body.password;
    const userId        = req.body.userId;
    const passwordToken = req.body.passwordToken;
    let resetUser;

    User.findOne({ resetToken: passwordToken, resetTokenExpiration: { $gt: Date.now() }, _id: userId })
        .then( user => {
            resetUser = user;
            return bcrypt.hash( newPassword, 12 );
        })
        .then( hashedPassword => {
            resetUser.password = hashedPassword;
            resetUser.resetToken = undefined;
            resetUser.resetTokenExpiration = undefined;
            return resetUser.save();
        })
        .then( result => {
            res.redirect( "/login" );
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
}
