const path                  = require( "path" );

const { validationResult }  = require( "express-validator/check" );

const fileHelper            = require( path.join( __rootPath, "util", "file" ));

const Product               = require( path.join( __rootPath, "models", "product" ));

// /admin/add-product => GET
exports.getAddProduct = ( req, res, next ) => {
    switch( __renderEngine ){
        case "pug":
        case "ejs":
            res.render( "admin/edit-product", {
                pageTitle: "Add product",
                path: "/admin/add-product",
                editing: false,
                hasError: false,
                errorMessage: null,
                validationErrors: []
            });
            break;

        case "hbs":
            res.render( "admin/edit-product", {
                pageTitle: "Add product",
                activeAddProduct: true,
                formsCSS: true,
                productCSS: true,
                hasError: false,
                errorMessage: null,
                validationErrors: []
            });
            break;

        default:
            console.error( "Render engine must be specified!" );
    }
};


// /admin/add-product => POST
exports.postAddProduct = ( req, res, next ) => {
    const title         = req.body.title;
    const image         = req.file;
    const price         = req.body.price;
    const description   = req.body.description;
    if( !image ){
        switch( __renderEngine ){
            case "pug":
            case "ejs":
                return res.status( 422 ).render( "admin/edit-product", {
                    pageTitle: "Add product",
                    path: "/admin/add-product",
                    editing: false,
                    hasError: true,
                    errorMessage: "Attached file is not an image.",
                    validationErrors: {
                        title:          "",
                        price:          "",
                        description:    ""
                    },
                    product: {
                        title: title,
                        price: price,
                        description: description
                    }
                });
                break;

            case "hbs":
                return res.status( 422 ).render( "admin/edit-product", {
                    pageTitle: "Add product",
                    activeAddProduct: true,
                    formsCSS: true,
                    productCSS: true,
                    editing: false,
                    hasError: true,
                    errorMessage: "Attached file is not an image.",
                    validationErrors: {
                        title:          "",
                        price:          "",
                        description:    ""
                    },
                    product: {
                        title: title,
                        price: price,
                        description: description
                    }
                });
                break;

            default:
                console.error( "Render engine must be specified!" );
        }
    }

    const errors        = validationResult( req );

    if( !errors.isEmpty()){
        const errorsArray = errors.array();
        switch( __renderEngine ){
            case "pug":
            case "ejs":
                return res.status( 422 ).render( "admin/edit-product", {
                    pageTitle: "Add product",
                    path: "/admin/add-product",
                    editing: false,
                    hasError: true,
                    errorMessage: errors.array()[ 0 ].msg,
                    validationErrors: {
                        title:          errorsArray.find(   e => e.param === "title" )          ? "invalid" : "",
                        price:          errorsArray.find(   e => e.param === "price" )          ? "invalid" : "",
                        description:    errorsArray.find(   e => e.param === "description" )    ? "invalid" : ""
                    },
                    product: {
                        title: title,
                        price: price,
                        description: description
                    }
                });
                break;

            case "hbs":
                return res.status( 422 ).render( "admin/edit-product", {
                    pageTitle: "Add product",
                    activeAddProduct: true,
                    formsCSS: true,
                    productCSS: true,
                    editing: false,
                    hasError: true,
                    errorMessage: errors.array()[ 0 ].msg,
                    validationErrors: {
                        title:          errorsArray.find(   e => e.param === "title" )          ? "invalid" : "",
                        price:          errorsArray.find(   e => e.param === "price" )          ? "invalid" : "",
                        description:    errorsArray.find(   e => e.param === "description" )    ? "invalid" : ""
                    },
                    product: {
                        title: title,
                        price: price,
                        description: description
                    }
                });
                break;

            default:
                console.error( "Render engine must be specified!" );
        }
    }

    const imageUrl = "/" + image.path;

    //If whole user object is stored in userId, mongoose knows to pick up _id.
    new Product({ title: title, price: price, description: description, imageUrl: imageUrl, userId: req.user._id }).save()
        .then( result => {
            res.redirect( "/admin/products" );
        })
        .catch( err => {
            // res.redirect( "/500" );
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );                                               //Calling next() with error as argument express will skip all middlewares and move to error handling middleware. Error catching middleware has error as first argument followed by req, res, next.
        });
}



// /admin/edit-product/:productId?edit=true => GET
exports.getEditProduct = ( req, res, next ) => {
    const editMode = req.query.edit == "true";
    if( !editMode )
        return res.redirect( "/" );

    const productId = req.params.productId;
    Product.findById( productId )
        .then( product => {
            if( !product )
                //BETTER TO SHOW ERROR INSTEAD
                return res.redirect( "/" );

            switch( __renderEngine ){
                case "pug":
                case "ejs":
                    res.render( "admin/edit-product", {
                        pageTitle: "Edit product",
                        path: "/admin/edit-product",
                        editing: editMode,
                        hasError: false,
                        errorMessage: null,
                        product: product,
                        validationErrors: []
                    });
                    break;

                case "hbs":
                    res.render( "admin/edit-product", {
                        pageTitle: "Edit product",
                        formsCSS: true,
                        productCSS: true,
                        editing: editMode,
                        hasError: false,
                        errorMessage: null,
                        product: product,
                        validationErrors: []
                    });
                    break;

                default:
                    console.error( "Render engine must be specified!" );
            }
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};



// /admin/edit-product/:productId?edit=true => GET
exports.postEditProduct = ( req, res, next ) => {
    const productId     = req.body.productId;
    const title         = req.body.title;
    const image         = req.file;
    const price         = req.body.price;
    const description   = req.body.description;

    const errors        = validationResult( req );
    if( !errors.isEmpty()){
        const errorsArray = errors.array();
        switch( __renderEngine ){
            case "pug":
            case "ejs":
                return res.status( 422 ).render( "admin/edit-product", {
                    pageTitle: "Edit product",
                    path: "/admin/edit-product",
                    editing: true,
                    hasError: true,
                    errorMessage: errors.array()[ 0 ].msg,
                    validationErrors: {
                        title:          errorsArray.find(   e => e.param === "title" )          ? "invalid" : "",
                        imageUrl:       errorsArray.find(   e => e.param === "imageUrl" )       ? "invalid" : "",
                        price:          errorsArray.find(   e => e.param === "price" )          ? "invalid" : "",
                        description:    errorsArray.find(   e => e.param === "description" )    ? "invalid" : ""
                    },
                    product: {
                        _id: productId,
                        title: title,
                        price: price,
                        description: description
                    }
                });
                break;

            case "hbs":
                return res.status( 422 ).render( "admin/edit-product", {
                    pageTitle: "Edit product",
                    formsCSS: true,
                    productCSS: true,
                    editing: true,
                    hasError: true,
                    errorMessage: errors.array()[ 0 ].msg,
                    validationErrors: {
                        title:          errorsArray.find(   e => e.param === "title" )          ? "invalid" : "",
                        imageUrl:       errorsArray.find(   e => e.param === "imageUrl" )       ? "invalid" : "",
                        price:          errorsArray.find(   e => e.param === "price" )          ? "invalid" : "",
                        description:    errorsArray.find(   e => e.param === "description" )    ? "invalid" : ""
                    },
                    product: {
                        _id: productId,
                        title: title,
                        price: price,
                        description: description
                    }
                });
                break;

            default:
                console.error( "Render engine must be specified!" );
        }
    }

    Product.findById( productId )
    .then( product => {
        if( product.userId.toString() !== req.user._id.toString() ){
            return res.redirect( "/" );
        }
        product.title       = title;
        product.price       = price;
        if( image ){
            fileHelper.deleteFile( product.imageUrl );
            product.imageUrl    = "/" + image.path;
        }
        product.description = description;
        return product.save()
            .then( result => {
                res.redirect( "/admin/products" );
            })
    })
    .catch( err => {
        const error = new Error( err );
        error.httpStatusCode = 500;
        return next( error );
    });
};


// /admin/products => GET
exports.getProducts = ( req, res, next ) => {
    Product.find({ userId: req.user._id })
    //.populate( "userId", "name" )                                             //Populates user object in place of userId. Optional second argument works like select.
    //.select( "title price -_id")                                              //Describes wanted fields. - is used to exclude.
        .then( products => {
            switch( __renderEngine ){
                case "pug":
                case "ejs":
                    res.render( "admin/products", {
                        pageTitle: "Admin Products",
                        path: "/admin/products",
                        products: products
                    });
                    break;

                case "hbs":
                    res.render( "admin/products", {
                        pageTitle: "Admin Products",
                        products: products,
                        hasProducts: products.length > 0,
                        activeAdminProducts: true,
                        productCSS: true
                    });
                    break;

                default:
                    console.error( "Render engine must be specified!" );
            }
        })
        .catch( err => {
            const error = new Error( err );
            error.httpStatusCode = 500;
            return next( error );
        });
};



// /admin/product/:productId => DELETE
exports.deleteProduct = ( req, res, next ) => {
    const productId = req.params.productId;
    Product.findById( productId )
        .then( product => {
            if( !product ){
                return next( new Error( "Product not found!" ));
            }
            fileHelper.deleteFile( product.imageUrl );
            return Product.deleteOne({ _id: productId, userId: req.user._id });
        })
        .then(() => {
            res.status( 200 ).json({ message: "Success!" });
        })
        .catch( err => {
            res.status( 500 ).json({ message: "Deleting product failed!" });
        });
};
