FROM node:alpine

WORKDIR /var/www/webshop

COPY ./package*.json ./

RUN npm install --production

COPY ./ ./

CMD ["npm", "start"]
