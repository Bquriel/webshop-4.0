const path      = require( "path" );

const multer    = require( "multer" );
const uuidv4    = require( "uuid/v4" );


const fileStorage = multer.diskStorage({                                        //Storage engine
    destination: ( req, file, callback ) => {                                   //If function instead of string is passed, folder must already exist on windows.
        callback( null, "img" );                                                //First argument is error. Null tells it's okay to proceed
    },
    filename: ( req, file, callback ) => {
        callback( null, uuidv4() + path.extname( file.originalname ));          //Extension (.png, .jpg ...) is kept in original name. Part or whole name should be hashed to ensure uniqueness.
    }
});


const fileFilter = ( req, file, callback ) => {
    if( file.mimetype === "image/png" || file.mimetype === "image/jpg" || file.mimetype === "image/jpeg" ){
        callback( null, true )                                                  //If file can be stored.
    } else {
        callback( null, false )                                                 //If file can't be stored.
    }
}

//Parses files. With this approach singe file named image is expected. Stores received data in req.file.
//Result will be in req.file.
module.exports = multer({                                                       //Object is optional. If not provided data will be stored in buffer.
     // dest: "img"                                                             //Where image is stored from project root. Doesn't add filetype to the end.
    storage: fileStorage,
    fileFilter: fileFilter                                                      //Filters which filetypes are allowed.
}).single( "image" );
