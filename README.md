# Webshop
Udemy course project with lots of personal twist.

## To use
Clone project and run *docker-compose up*
**OR** 
*npm i* and *npm start*
Then go to browser and open localhost:9002

You can also find online version https://webshopp.rhyv.space/

Database credentials are used for testing and all IP's are allowed.