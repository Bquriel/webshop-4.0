const fs = require( "fs" );

//TODO: Implement non-crashing error handling if file is missing.
const deleteFile = ( filePath ) => {
    filePath = __rootPath + filePath;
    fs.unlink( filePath, ( err ) => {
        if( err ){
            throw( err );
        }
    })
};


exports.deleteFile = deleteFile;
